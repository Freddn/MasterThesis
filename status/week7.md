# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 

## Initial plan for the week

Investigate the requirements on exchanges to use the Lightning Networks.  More simulations. Write on method and simulation.

## What has been done

This week a lot of tool and API exploring has been done. I've built several scripts that next week will be assembled into an implementation. 

I've written 4-5 pages of introduction/background and theory on the thesis so far. 

## Problems

Some problems with security protocols and keys have occured. For now solved and the security can be omitted when testing as well. 

## Experiences

I believe the requirements on exchanges cannot be investigated now. It will be a later concern. I will worry less about the exchanges and focus more on the technical aspects. 

## Communications

I had a meeting with the advisors at cinnober. This meeting made things clearer and we descided that I should focus on building an implementation next week.

## Time status

Next week I will build an implementation. This implementation can be used to evaluate the usabillity of the protocol. This implementation should not take to much time and a simple version should be done week 9. What I will build is a client and a server where the client can deposit and withdraw funds from the server with the help of a web interface. 


