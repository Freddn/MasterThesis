# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week
Prepare the opposition. Prepare for final submission of thesis related documents. Prepare for the presentation and write the opposition report. 


## What has been done

Have been writing the opposition report, writing opposition questions and working on the presentation. And now preparing the diary for submission. 

## Problems
-

## Experiences
-

## Communications
-

## Time status

All good. 
