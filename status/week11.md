# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week

Evauluate the prototype and write on the evaluation and discussion. Meeting with advisor. 

## What has been done

Meeting with advisor and discussed the structure of the thesis and different approaches to the evaulation. 


## Problems

Not clear which of security, privacy or ease of use should have most value in the evaluation and which of them I should put most time in. 

I have been writing on different parts and jumping between topics all the time. This was not so effective and I should start to focus on one section at a time. 

## Experiences

Not so effective to jump between different topics when writing. 


## Communications

A meeting with my UmU advisor then later a meeting with the advisors at Cinnober. 


## Time status

Good for now. 


