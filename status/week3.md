# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week


Work on the project plan. Find and read existing literature about lightning networks.

## What has been done

This week I’ve been reading a lot of resources on lightning. I’ve read the whole white paper on lightning (58 pages) and it feels like my understanding of the topic is somewhat up to date now. Other that the white paper some interesting articles and posts on the topic has been read which I believe are important for the thesis. 


## Problems
I’m lacking an advisor from UmU and I’ve had no meeting with my advisor from Stockholm yet. I need to have a meeting with both my university advisor and my advisor from Stockholm and discuss the questions for the thesis. An email conversation will work as well but will take longer to come to an understanding and reach agreement. 

## Experiences
I have found out that the questions for the thesis is hard to formulate. Not that much literature about the topic but some good has been found. This is a somewhat new area and much of the information I believe can be found in places like blogs, articles and reddit/forum/discussion posts. 

## Communications
I’ve had email contact with Henrik Björklund who approved the thesis specification proposal. No advisor has been selected for me yet. 
Eric Wall contacted me regarding a meeting and talked about an additional topic that can be included in the thesis. 
Oskar and Jonas have reviewed the project proposal and prepared setting up a location for me at Cinnober. 

## Time status
The first week of the project has passed but time is not a problem at the moment. I’ve read some key literature and articles which has given me a good foundation for completing this thesis. 


