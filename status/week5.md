# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week

Information about exchanges needed in the investigation. Find out what tools to use for simulation. Experiment and learn the tools for using/testing the lightning network. 


## What has been done

I have set up LND which is an implementation following the LN protocol. The LND implementation can be used to simulate the network. Using the simulation network was simple.  

I have written a skeleton thesis and started writing on the introduction/background based the found litterature. 

## Problems
It is not clear how I should set up the simulations. Learning more about the user case and need might help on this matter. For better understanding I suggest creating user stories for helping specifying the simulations. 

## Experiences
-

## Communications

Some good information has been shared by advisors from Cinnober. 


## Time status

I have a lot to write on the introduction/background if I get stuck with the simulation so the time is not a problem yet. 



