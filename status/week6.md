# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week

Continue to experiment with the lightning network tools and try to simulate basic use cases. Write on method/simulation. Write on the investigation.

## What has been done

I've done a lot of thinking on the simulation and got some questionmarks erased when discussed with an advisor. The simulations will need a bit of implementation to work. On the report I've written mostly on the Theroy and Background part. I also read about scaling solutions for the payment channels.

## Problems

Had problems understanding how to approac the simulation/implementation. Now I at least have a plan where to start. Another problem is that we do not have a clear problem statement and what excatly the thesis will solve. This I hope will be more clear when some simulations have been done. 

## Experiences
-

## Communications
Joined a forum for the payment channel (Lightning Network). Discussed on Slack with my advisors at cinnober. 

## Time status
All good. 

