# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week
The initial plan for this week was to reflect over the status of the project with the help of the peer review. The peer review did not give any feedback needing any major changes. The feedback was that the project could benefit using more figures and using references. We mostly discussed the technical aspects of Bitcoin and the Lightning network. 

## What has been done
As the peer review did not result in any major changes I've worked a lot on the thesis report. Most work has been done on the general Theory and Theory of Lightning network. I've been able to reference a lot of sources in the text. I have also found many new sources that I can use.

## Problems

How to properly reference articles and technical reports has been a problem, not that it's a big problem. Also geting the references to show up in the order of reference seems to be problematic in Latex. 

A lot of links and other bloat end up in the thesis. Somewhat hard to keep track on all the information I've found. A separate document with TODOs, sources and thoughts maby would be nice to have. 

## Experiences


## Communications
Some brief communications with my advisors on cinnober but nothing of great importance. 

## Time status
Doing ok for now. 



