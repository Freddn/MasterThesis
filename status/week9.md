# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week
Build the prototype and prepare for peer review. 

Next week I will document the design decisions and problems found for the protoype. 

## What has been done

The prototype is now built and a demo had been show to the external advisors. I've also read up on the work my group has submitted to me and prepared some questions. 


## Problems
No, demo went well. Some technical problems before the demo and under the demo but none that couldn't be handled.

## Experiences
You can always prepare more. 

## Communications

Some discussions after the demo provided some guidelines on what to focus on now. 

## Time status

On time. 


