# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week
Finish the report. 

## What has been done

Final polishing. Wrote the abstract and did some final error corrections. 

## Problems
It was somewhat unclear if all questions was answered. This was solved with additional information in the report and removal of one question that was not really touched upon and would not bring any addition to the results. 

## Experiences

## Communications
Sent the report to Mikael Rännar for final review. Mikael Rännar thought it looked good. 

## Time status


