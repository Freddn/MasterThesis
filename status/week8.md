# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week
I have been revidating the project plan a bit. The plan for this week was to build a prototype. The prototype has some work left on it and but I set a maximum of 2 weeks total for the prototype. 

## What has been done
A good amount of work has been done on the prototype. The prototype has a web interface and an SQL database that almost is finnished.


## Problems
None yet. Setting up database and frontend is not such a fast process but I've restricted the prototype to bare minimum. The implementation of the protocol is written in golang which I've never dealt with. This slows down the process of understanding what is going on. 

## Experiences
The community around LND which is the implementation of the Lightning Network I currently use is really helpful. There are a lot of developers exploring the protocol now so there are a lot of activity. 

## Communications
I've discussed the implementation with my advisors and they think that the prototype is a good way to evaluate the protocol and they provided many good insights. 

## Time status
If the prototype only take 5 more work days I will have a lot of time to evaluate it.



