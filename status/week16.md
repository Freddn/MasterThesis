# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week

Prepare for peer review and work on evaluation. 


## What has been done

Same as planned. Not so much left on the evaluation. Have worked on improving the text flow and background/theory as well. 

## Problems
Outer factors like job interviews have been bad for my focus and time. Will not be as much of that from now on. 

## Experiences


## Communications


## Time status

Starting to get closer the deadline. Still work to do on the evaluation but the discussion and conclusion will be faster to finish. 


