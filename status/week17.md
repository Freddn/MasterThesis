# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 

## Initial plan for the week

The initial plan for this week was to reflect on feedback from the peer review. 

## What has been done

On the peer review we was one man short and another one had more important things to do so the feedback was very limited. The feedback given was that it looked good and relevant things were brough up.

## Problems
I have some difficulties with where I should put certain sections. 

## Experiences
There seem to be no limit on how much one can polish certain parts the report. 

## Communications

Other than the peer review I had a meeting with Eric from the stockholm office discussing Lightning in general and shared some insights. Then my advisors at cinnober and I had a meeting discussing the status of the thesis and what work is left.

## Time status

I will send my work to my advisor at Umeå university and hear what he think the status of the thesis is. It is hard to estimate how much is left and how much work can be skipped and therefor it is esential that I can have a discussion with the UmU advisor before it is too late. 




