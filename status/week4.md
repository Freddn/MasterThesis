# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week

More literature studies. Read up on cryptocurrencies-exchanges. Start writing on introduction/background and a skeleton for the rest. Meet with UmU advisor and Cinnober advisor. Finnish project plan. 


## What has been done

I've read a lot of articles and new information. I've joined a chat community that will be valuable when running implementations for the simulations. I've started to plan the looks of the thesis and what chapters are needed. I've had a meeting with all my advisors (Oskar, Jonas, Eric & Mikael) during the week.

## Problems

Before the meeting with the advisors at cinnober I didn't really know the direction of the work and what to focus on specifically. Now thats solved and I have a plan for now. 

## Experiences
-

## Communications
Meetings with all advisors. 
## Time status
A lot of time left.

