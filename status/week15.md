# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week

Finnish tables and diagrams if there are any. Work on report (Abstract and Conclusion).

Abstract should be done later.. Not enough evaluation for the conclusion. 

## What has been done

I've worked on making the report easier to read, added figures and removed redundant comments and links. No work on abstract or conclusion has been done. The work on conclusion and discussion should be started soon. 

## Problems

## Experiences

## Communications

Had a meeting with the advisors at Cinnober discussing the status and some feedback on the report. 

## Time status
Need to finish evaluation really soon so that I can start with the results and conclusion. The discussion should also be started... 

