# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week
Write on the overall report. Look over the style and references of the paper. 

## What has been done

Alot of refinements have been done. Have been checking if things are described enough and that the information is correct. I have also incorporated feedback from Oskar and Jonas from Cinnober who gave me feedback on errors in the report and areas that need to be described better. 




## Problems
I found it hard to prioritize between adding more information and topics to the report or improving the existing parts. 

## Experiences


## Communications

Had a meeting with Mikael Rännar and the feedback was that the work did not need more information but may be in need of more clarifications and error corrections. 

## Time status
After the meeting with Mikael the time status seems really good and I can work a lot on improvements and error correction. 


