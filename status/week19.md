# Project plan

In this document the work planned for the week will be described and what work actually got done after the week had passed. 


## Initial plan for the week
Write on the overall report. 

## What has been done
I decided that the report need a better explanation on the HTLC or payment forwarding. I made some major changes because some information about the payment forwarding was outdated. I drew a lot of images for the report and removed a lot of unnecessary information. I also did a lot of work filling out needed sections while also correcting errors. 


## Problems


## Experiences
It is way to easy to find new things that would make the report better. It is also somewhat hard to remove sections or parts of sections that is not really needed in the report. 



## Communications

Got some more feedback from Jonas and Oskar, mostly errors in the text. 

## Time status

All good. 


